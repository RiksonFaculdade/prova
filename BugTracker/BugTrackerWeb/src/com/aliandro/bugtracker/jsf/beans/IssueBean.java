package com.aliandro.bugtracker.jsf.beans;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.aliandro.bugtracker.model.BugTracker;
import com.aliandro.bugtracker.model.Issue;

@Named
@RequestScoped
public class IssueBean {

	private Issue issue;
	
	@EJB
	private BugTracker tracker;
	
	public IssueBean() {
		this.issue = new Issue();
	}
	
	public Issue getIssue() {
		return issue;
	}

	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	public String save(){
		tracker.addIssue(this.issue);
		return "sucesso";
	}
	
	
}
