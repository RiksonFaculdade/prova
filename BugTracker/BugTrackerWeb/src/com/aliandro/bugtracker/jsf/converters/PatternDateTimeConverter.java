package com.aliandro.bugtracker.jsf.converters;

import java.util.TimeZone;

import javax.faces.convert.DateTimeConverter;
import javax.faces.convert.FacesConverter;

import org.joda.time.DateTimeZone;

@FacesConverter(forClass=java.util.Date.class)
public class PatternDateTimeConverter extends DateTimeConverter {

	public PatternDateTimeConverter() {
		super();
		this.setPattern("dd/MM/yyyy");
		setTimeZone(TimeZone.getTimeZone(DateTimeZone.UTC.getID()));;
	}
	
}
